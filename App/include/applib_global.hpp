#ifndef APPLIB_GLOBAL_HPP
#define APPLIB_GLOBAL_HPP

#include <QtCore/qglobal.h>

#if defined(APPLIB_LIBRARY)
#define APPLIB_EXPORT Q_DECL_EXPORT
#else
#define APPLIB_EXPORT Q_DECL_IMPORT
#endif

#endif // APPLIB_GLOBAL_HPP
