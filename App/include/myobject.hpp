#ifndef MYOBJECT_HPP
#define MYOBJECT_HPP

#include "applib_global.hpp"
#include <QCoreApplication>
#include <QDebug>
#include <QObject>

namespace MyNameSpace {
class APPLIB_EXPORT MyObject : public QObject {
  Q_OBJECT
private:
  int currCount;
  bool signaled;

public:
  explicit MyObject(QObject *parent = 0);
  ~MyObject();
  int getCount();
  bool isSignaled();
  void count();
  void printHello();

public slots:
  void testSlot();

signals:
  void testSignal();
};

} // namespace MyNameSpace

#endif // MYOBJECT_HPP
