#include "myobject.hpp"

using namespace MyNameSpace;

MyObject::MyObject(QObject *parent) : QObject(parent) {
  this->currCount = 0;
  this->signaled = false;
}

MyObject::~MyObject() {
  qDebug() << "My Object Deleted!";
  QCoreApplication::quit();
}

int MyObject::getCount() { return this->currCount; }

bool MyObject::isSignaled() { return this->signaled; }

void MyObject::count() { this->currCount++; }

void MyObject::printHello() {
#ifdef Q_OS_WIN
  qDebug() << "Hello from windows!";
#elif defined(Q_OS_UNIX)
  qDebug() << "Hello from Mac/Linux!";
#else
  qDebug() << "Hello from unknown!"
#endif
}

void MyObject::testSlot() { this->signaled = true; }
