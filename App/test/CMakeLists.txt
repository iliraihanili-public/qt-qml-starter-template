project(App-test)

set(TEST_HEADERS
    include/testrunner.hpp
    include/myobjecttest.hpp
)

set(TEST_SOURCES
    src/main.cpp
    src/testrunner.cpp
    src/myobjecttest.cpp
)

add_executable(${PROJECT_NAME} ${TEST_HEADERS} ${TEST_SOURCES})

target_link_libraries(${PROJECT_NAME} PRIVATE
    Qt6::Core
    Qt6::Test
    ${LIBRARY}
)
target_include_directories(${PROJECT_NAME} PUBLIC include)

add_test(
    NAME ${PROJECT_NAME}
    COMMAND ${PROJECT_NAME}
)
