#ifndef MYOBJECTTEST_HPP
#define MYOBJECTTEST_HPP

#include <QObject>

class MyObjectTest : public QObject {
  Q_OBJECT

private slots:
  void initTestCase();
  void cleanupTestCase();
  void init();
  void cleanup();
  void testConstructor();
  void testCount();
  void testSignal();
};

#endif // MYOBJECTTEST_H
