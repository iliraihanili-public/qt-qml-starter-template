#ifndef TESTRUNNER_HPP
#define TESTRUNNER_HPP

#include <QObject>

class TestRunner {
public:
  TestRunner(int argc, char *argv[]);
  int runTests();

private:
  int m_argc;
  char **m_argv;
};

#endif // TESTRUNNER_HPP
