#include <QCoreApplication>
#include <QTest>

#include "testrunner.hpp"

int main(int argc, char *argv[]) {
  TestRunner testRunner(argc, argv);
  return testRunner.runTests();
}
