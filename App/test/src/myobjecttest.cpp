#include "myobjecttest.hpp"
#include "myobject.hpp"
#include "qsignalspy.h"

using namespace MyNameSpace;

void MyObjectTest::initTestCase() {}

void MyObjectTest::cleanupTestCase() {}

void MyObjectTest::init() {}

void MyObjectTest::cleanup() {}

void MyObjectTest::testConstructor() {
  MyObject obj;
  QCOMPARE(obj.getCount(), 0);
  QVERIFY(!obj.isSignaled());
}

void MyObjectTest::testCount() {
  MyObject obj;

  for (int i{0}; i < 3; i++)
    obj.count();

  QCOMPARE(obj.getCount(), 3);
}

void MyObjectTest::testSignal() {
  MyObject obj;
  QSignalSpy spy(&obj, SIGNAL(testSignal()));

  QObject::connect(&obj, &MyObject::testSignal, &obj, &MyObject::testSlot);
  emit obj.testSignal();

  QVERIFY(obj.isSignaled());
  QCOMPARE(spy.count(), 1);
}
