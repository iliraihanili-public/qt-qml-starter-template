#include "testrunner.hpp"
#include <QtTest>

#include "myobjecttest.hpp"

TestRunner::TestRunner(int argc, char *argv[]) : m_argc(argc), m_argv(argv) {}

int TestRunner::runTests() {
  QCoreApplication app(m_argc, m_argv);

  MyObjectTest test;

  return QTest::qExec(&test, m_argc, m_argv);
}
