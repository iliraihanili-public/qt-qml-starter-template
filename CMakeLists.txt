cmake_minimum_required(VERSION 3.16)

set(PARENT_PROJECT_VERSION_MAJOR 0)
set(PARENT_PROJECT_VERSION_MINOR 1)
set(PARENT_PROJECT_VERSION ${PARENT_PROJECT_VERSION_MAJOR}.${PARENT_PROJECT_VERSION_MINOR})

project(qt-qml-starter-template VERSION ${PARENT_PROJECT_VERSION} LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt6 REQUIRED COMPONENTS Quick Core Test)

if(MSVC) # Remove this condition if manual export prefix definations are preferred.
    set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS TRUE)
    set(BUILD_SHARED_LIBS TRUE)
endif()

enable_testing()

add_subdirectory(App)
