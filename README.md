# qt-widget-starter-template

## Getting started

The following is a starter template for QT6 qml application. It comes in a sub directory style layout along with Qt test.

## VSCode

The project will work in vscode provided you have the Microsoft CPP extension along with CMake and CMake Tools extensions installed. Here's a sample launch.json content:

    "version": "0.2.0",
    "configurations": [
        {
            "name": "(Windows) Launch",
            "type": "cppvsdbg",
            "request": "launch",
            "program": "${command:cmake.launchTargetPath}",
            "visualizerFile": "${workspaceFolder}/qt6.natvis.xml",
            "args": [],
            "stopAtEntry": false,
            "cwd": "${workspaceFolder}"
        }
    ]

### Debugging in VSCode

The repo comes with the qt6 navtis taken from KDAB, however the navtis file doesn't work so well with gdb and lldb. So for windows(GCC), linux and mac its recommended to use QTCreator to debug code.
